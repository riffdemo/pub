import pika
import sys
from time import sleep

channel = None
while True:
    try:
        with open("ip.txt") as f:
            ip = f.readlines()[0]
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(host=ip))
        channel = connection.channel()
        if (channel is not None):
            break
    except pika.exceptions.AMQPConnectionError:
        sleep(1)

channel.exchange_declare(exchange='logs', exchange_type='fanout')
print(' [*] Waiting for input. To exit press CTRL+C')
try:
    while True:
        message = input()
        channel.basic_publish(exchange='logs', routing_key='', body=message)
        #print(" [x] Sent %r" % message)
except KeyboardInterrupt:
    connection.close()
    print('Interrupted')
    sys.exit(0)